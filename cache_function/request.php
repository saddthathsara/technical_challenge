<?php

require_once 'vendor/autoload.php';

class SimpleJsonRequest
{
    private static function makeRequest(string $method, string $url, array $parameters = null, array $data = null)
    {

        $opts = [
            'http' => [
                'method' => $method,
                'header' => 'Content-type: application/json',
                'content' => $data ? json_encode($data) : null
            ]
        ];
        $url .= ($parameters ? '?' . http_build_query($parameters) : '');
        return file_get_contents($url, false, stream_context_create($opts));
    }

    public static function get(string $url, array $parameters = null)
    {
        $response = Cache::get('simpleJsonRequest', function () use ($url, $parameters) {
            return json_decode(self::makeRequest('GET', $url, $parameters));
        });
        return $response;

    }

    public static function post(string $url, array $parameters = null, array $data)
    {
        $response = Cache::post('simpleJsonRequest', function () use ($url, $parameters, $data) {
            return json_decode(self::makeRequest('POST', $url, $parameters, $data));
        });
        return $response;
    }

    public static function put(string $url, array $parameters = null, array $data)
    {
        $response = Cache::update('simpleJsonRequest', function () use ($url, $parameters, $data) {
            return json_decode(self::makeRequest('PUT', $url, $parameters, $data));
        });
        return $response;
    }

    public static function patch(string $url, array $parameters = null, array $data)
    {
        $response = Cache::update('simpleJsonRequest', function () use ($url, $parameters, $data) {
            return json_decode(self::makeRequest('PATCH', $url, $parameters, $data));
        });
        return $response;

    }

    public static function delete(string $url, array $parameters = null, array $data = null)
    {
        //remove cache for specific key
        Cache::delete('simpleJsonRequest');
        return json_decode(self::makeRequest('DELETE', $url, $parameters, $data));
    }

}

//Cache Class
class Cache
{
    //Create redis client object
    public static function redisClient()
    {
        $client = new Predis\Client('tcp://127.0.0.1:6379');
        return $client;
    }

    //Generate Redis Key (We can use this function to generate a key but here I'm using simpleJsonRequest as key )
    public static function key(string $url, array $parameters = [])
    {
        return md5($url . http_build_query($parameters));
    }

    //Set Value in Redis
    public static function set(string $key, $value, $ttl)
    {

        self::redisClient()->setex($key, $ttl, $value);
    }

    //Get value from Redis
    public static function get(string $key, $callback)
    {

        //Get the value according to redis key
        $value = self::redisClient()->get($key);

        //Check value exist
        if ($value == null) {

            $callbackValue = $callback();
            self::set($key, $callbackValue, 3600);
            return $callbackValue;
        } else {
            return $value;
        }
    }

    //For Post Methods
    public static function post(string $key, $callback)
    {

        //Get the value according to redis key
        $value = self::redisClient()->get($key);

        //Check value exist
        if ($value == null) {

            $callbackValue = $callback();
            self::set($key, $callbackValue, 3600);
            return $callbackValue;
        } else {
            return $value;
        }
    }

    //Update value from Redis
    public static function update(string $key, $callback)
    {
        //Get the value according to redis key
        $value = self::redisClient()->get($key);
        $callbackValue = $callback();
        //Check value exist
        if ($value != null) {
            self::delete($key);
            self::set($key, $callbackValue, 3600);
            return $callbackValue;
        }else{
            self::set($key, $callbackValue, 3600);
            return $callbackValue;
        }
    }

    //Delete Value in Redis
    public static function delete(string $key)
    {
        self::redisClient()->del($key);
    }

}

//Test methods
$url = 'http://google.com';
$parameters = ['one' => 'two7'];
$data=['hello','world'];
$test = SimpleJsonRequest::get($url, $parameters);

