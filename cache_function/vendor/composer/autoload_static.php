<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit80448b9bb1c244e3ec1215cfc091b109
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Predis\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Predis\\' => 
        array (
            0 => __DIR__ . '/..' . '/predis/predis/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit80448b9bb1c244e3ec1215cfc091b109::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit80448b9bb1c244e3ec1215cfc091b109::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
